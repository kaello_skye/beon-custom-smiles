// ==UserScript==
// @name Custom Smiles Toolbar for Beon.ru
// @namespace beonCustomSmiles
// @version 1.2
// @author KaelloSkye (http://ryuuga.beon.ru)
// @description Тулбар с кастомными смайликами для beon.ru
// 
// Скрипт, добавляющий тулбар с кастомными смайлами-колобками 
// к формам на сайте Beon.ru. Смайлики добавляются непосредственно 
// с сайта kolobok.us
// 
// Все смайлы принадлежат aiwan (kolobok.us)
// 
// Идея скрипта нагло украдена у gh0strider aka engineer aka vlad_z 
// (http://iamrider.beon.ru)
// 
// Действует для любых форм добавления новой темы или комментария;
// Смайлики выводятся в несколько строчек;
// Блок смайликов автоматически скрыт для экономии места, его можно скрыть/показать в любое время;
// В личных сообщениях пока работает только в первом открытом диалоге;
// На темном фоне колобки смотрятся не самым лучшим образом.
// 
// @match        *://*.beon.ru/*
// @grant       none

// ==/UserScript==

// список объектов с описанием смайликов
var SMILES = [
    {src: 'smile.gif', alt: ':-)', title: ':-) smile'},
    {src: "rofl.gif", alt: ":-D", title: ":-D rofl"},
    {src: "acute.gif", alt: ";-D", title: ";-D acute"},
    {src: "air_kiss.gif", alt: ":-*", title: ":-* kissed"},
    {src: "blush.gif", alt: ":-[", title: ":-[ blush"},
    {src: "blum3.gif", alt: ":-P", title: ":-P blum"},
    {src: "angel.gif", alt: "O:-)", title: "O:-) angel"},
    {src: "beee.gif", alt: ":-d", title: ":-d beee"},
    {src: "bad.gif", alt: ":-m", title: ":-m bad"},
    {src: "boredom.gif", alt: ":-O", title: ":-O boredom"},
    {src: "crazy.gif", alt: "8-D", title: "8-D crazy"},
    {src: "cray.gif", alt: "8-(", title: "8-( cray"},
    {src: "dash1.gif", alt: "dash", title: "dash"},
    {src: "diablo.gif", alt: "diablo", title: "diablo"},
    {src: "give_heart.gif", alt: ":-)", title: ":-) give heart"},
    {src: "give_rose.gif", alt: "rose", title: "rose"},
    {src: "good.gif", alt: "good", title: "good"},
    {src: "hang2.gif", alt: "hang", title: "hang"},
    {src: "help.gif", alt: "help", title: "help"},
    {src: "i-m_so_happy.gif", alt: "happy", title: "happy"},
    {src: "kiss.gif", alt: "kiss", title: "kiss"},
    {src: "mosking.gif", alt: "mosking", title: "mosking"},
    {src: "music.gif", alt: "music", title: "music"},
    {src: "pardon.gif", alt: "pardon", title: "pardon"},
    {src: "secret.gif", alt: "secret", title: "secret"},
    {src: "scratch_one-s_head.gif", alt: "scratch", title: "scratch"},
    {src: "spiteful.gif", alt: "spiteful", title: "spiteful"},
    {src: "sorry.gif", alt: "sorry", title: "sorry"},
    {src: "swoon.gif", alt: "swoon", title: "swoon"},
    {src: "thank_you2.gif", alt: "thank", title: "thank you"},
    {src: "wink3.gif", alt: "wink", title: "wink"},
    {src: "yes3.gif", alt: "yes", title: "yes"}
];

// ссылка на каталог смайликов
var URL = 'http://kolobok.us/smiles/light_skin/';
// все виды форм
var ID = ['topic_form', 'comment_form', 'pm_form'];


// пробегаемся по списку ID, ищем формы и добавляем к ним тулбар
for(var i = 0; i < ID.length; i++) {
    //ищем и определяем форму из трех возможных типов
    var form = findForm(ID[i]);

    if(form) {
        // если нужная форма найдена на странице,
        // то ищем в ней тулбар со смайликами
        oldSmiles = findToolbar(form);
        // задаем глобальную переменную с идентификатором формы
        var context = form.id;
        // создаем новый тулбар с кастомными смайликами
        customSmiles = createCustomToolbar();
        // добавляем кастомные смайлы после оригинальных
        oldSmiles.parentNode.appendChild(customSmiles);
    }
}


function findForm(form_id) {
    // ищем на странице форму, идентификатор которой 
    // начинается с form_id, или возвращаем null
    return null || document.querySelector("form[id^="+form_id+"]");
}


function findToolbar(form) {
    // по типу формы определяем, какой из 2-ух
    // видов тулбаров на ней искать
    var toolbar = null;
    switch(form.id) {
        // если это форма нового поста или комментария,
        // то ищем тулбар с классом .toolbar
        case 'topic_form':
        case 'comment_form':
            toolbar = form.querySelector(".toolbar");
            break;
        // иначе это форма из личных сообщений, 
        // ищем тулбар с классом .pmtoolbar
        default:
            toolbar = form.querySelector(".pmtoolbar");
            break;
    }
    return toolbar;
}


function createCustomToolbar() {
    // функция создает и возвращает тулбар с рабочими кастомными смайлами
    // создаем общий блок тулбара
    var customSmilesToolbar = document.createElement('div');
    // устанавливаем ширину блока 
    customSmilesToolbar.style.width = '480px';
    // создаем блок для размещения смайликов
    var blockSmiles = document.createElement('div');    
    // сдвигиваем этот блок немного влево, чтобы убрать пустую область
    // отрицательный отступ, ммм, ужас верстальщика, наверно =)
    blockSmiles.style.marginLeft = '-20px'; 
    // и скрываем его с глаз долой
    blockSmiles.style.display = 'none';

    // пробегаемся по списку смайликов, добавляя
    // их внутрь блока blockSmiles
    SMILES.forEach(function(item, i, arr) {
        blockSmiles.appendChild(createSmileBtn(item));
    });
    
    // ссылка для управления видимостью блока смайлов
    var blockTitle = document.createElement('a');
    blockTitle.innerText = 'Показать анимированные смайлы';

    // оформление ссылки показать/скрыть:
    //       блочное отображение
    //       отступы сверху 10px, снизу 5px и 0 по бокам
    //       размер шрифта 12px
    //       подчеркивание
    //       курсор-указатель
    blockTitle.style.display = 'block';
    blockTitle.style.margin = '10px 0 5px 0';
    blockTitle.style.fontSize = '12px';
    blockTitle.style.textDecoration = 'underline';
    blockTitle.style.cursor = 'pointer';

    // вешаем на ссылку обработчик события по нажатию
    // для управления видимостью блока смайлов
    blockTitle.onclick = showHideSmilesBlock;

    // добавляем ссылку и блок смайлов на наш тулбар
    customSmilesToolbar.appendChild(blockTitle);
    customSmilesToolbar.appendChild(blockSmiles);
    
    // возвращаем сформированный тулбар
    return customSmilesToolbar;
}

function createSmileBtn(item) {
    // функция создает и возвращает ссылку с изображением
    // смайлика по шаблону:
    // <a href="addSmile('<идентификатор формы>', '<код изображения>');">
    //      <img src="<URL изображения>" alt="<альтернативный текст>" 
    //          title="<всплывающая подсказка>" />
    // </a>
    var link = document.createElement('a');
    link.href = "javascript:addSmile('" + context + "', '[image-original-none-" + URL + item.src + "] ');";

    // оформление смайлов: 
    //      строчно-блочное отображение,
    //      отступы по бокам и сверху снизу,
    //      ширина одного смайла
    //      и отображение картинки посередине блока
    link.style.display = 'inline-block';
    link.style.margin = '3px 5px';
    link.style.width = '50px';
    link.style.textAlign = 'center';
    
    var img = document.createElement('img');
    img.src = URL + item.src;
    img.alt = item.alt;
    img.title = item.title;
    link.appendChild(img);
    return link
}

function showHideSmilesBlock() {
    // находим блок со смайлами
    blockSmiles = this.parentNode.lastChild;
    state = blockSmiles.style.display;
    if(state == 'none') {
        // если значение свойства display равно none,
        // то блок невидим, делаем его видимым
        this.innerText = 'Скрыть анимированные смайлы';
        blockSmiles.style.display = 'block';
    } else {
        // иначе опять его скрываем
        this.innerText = 'Показать анимированные смайлы';
        blockSmiles.style.display = 'none';
    }
}