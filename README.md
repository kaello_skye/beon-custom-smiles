# Тулбар кастомных смайлов для BeOn.ru #
*Текущая версия: 1.2*

Скрипт, добавляющий тулбар с кастомными анимированными смайлами-колобками к формам на сайте Beon.ru. 
Под стандартными смайлами рисуется панель с дополнительными, при клике на выбранном колобке в поле сообщения вставляется код сайта с параметрами картинки. Смайлы видят все пользователи сайта, даже те, у кого не установлено дополнение.

![1.png](https://bitbucket.org/repo/jAz8xG/images/950645901-1.png) ![2.png](https://bitbucket.org/repo/jAz8xG/images/367293421-2.png)

Смайлики добавляются непосредственно с сайта [kolobok.us](http://kolobok.us)

Все смайлы принадлежат aiwan ([kolobok.us](http://kolobok.us))
 
Идея скрипта и часть описания нагло украдена у [gh0strider aka engineer aka vlad_z](http://iamrider.beon.ru). ;-)


### Немного о скрипте: ###
* Действует для форм добавления новой темы, комментария или личного сообщения;
* Смайлики выводятся в несколько строчек;
* Блок смайликов автоматически скрыт для экономии места, его можно показать/скрыть в любое время;
* Работает на страницах, где есть две и более нужные формы (например, личный дневник или сообщество);


### Проблемы: ###
* В личных сообщениях пока работает только в первом открытом диалоге;
* На темном фоне колобки смотрятся не самым лучшим образом.


### Установка: ###
1. Скачать расширение для браузера:
    * Firefox: [GreaseMonkey](https://addons.mozilla.org/ru/firefox/addon/greasemonkey/) или [TamperMonkey](https://addons.mozilla.org/ru/firefox/addon/tampermonkey/)
    * Chrome: [ViolentMonkey](https://chrome.google.com/webstore/detail/violent-monkey/jinjaccalgkegednnccohejagnlnfdag/reviews?hl=ru) или [TamperMonkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=ru)
    * Opera: [ViolentMonkey](https://addons.opera.com/ru/extensions/details/violent-monkey/?display=ru) или [TamperMonkey](https://addons.opera.com/ru/extensions/details/tampermonkey-beta/?display=en)
2. [Скачать](https://bitbucket.org/kaello_skye/beon-custom-smiles/downloads/BeonCustomSmiles.current.user.js) скрипт. При наличии необходимого расширения установка скрипта должна начаться автоматически.
3. Не забудьте включить скрипт в настройках вашего расширения